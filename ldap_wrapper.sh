#!/bin/bash
# 
# An assistance script to get certificates from SK LDAP repository
# -s for serial-, -n for namebased searches. If encoded DN is returned,
# script will automatically (base64-)decode it for human-readable 
# user experience.
#
# Tested w. OSX. For linux, atleast base64 -D should be changed (changed for 
# base64 -d). Possiblt other alterations might be needed.
#
# (c) 2018, Tiit Hallas
#

dosearch() {
	echo ""
	echo "Looking certificates with '$search'"
	echo "----------------------------"
	ldapsearch -o ldif-wrap=no -x -h ldap.sk.ee -b c=EE "$search" "dn" | grep "dn:"	| while read line; do 
		prefix=$(echo $line | cut -f1 -d ' ')
		payload=$(echo $line | cut -c4-1000) 

		if [[ $prefix == "dn::" ]]; then
			payload=$(echo $line | cut -f2 -d ' ' | base64 -D)
		fi
		echo $payload
	done

	echo "----------------------------"
	echo "Done."
	echo ""

}

usage() {
	echo ""
	echo " Script usage:"
	echo "   -n | --name     Common Name based search  (CN like *name*)"
	echo "   -s | --serial   Serialnumber based search (SerialNumber=serial)"
	echo ""
	echo " Example:"
	echo "   ./ldap.sh -s 123456"
	echo "   ./ldap.sh -n company"
	echo ""
}

if [[ "$#" -eq 2 ]]; then
search=""
	while [[ "$1" != "" ]]; do
	  case $1 in
	    -n | --name )   search="(cn=*$2*)"
	                    dosearch
	                    ;;
	    -s | --serial )	search="(serialNumber=$2)"
											dosearch
											;;
	    * )							usage
	                    exit 0
	  esac
	  exit 0
	done
else
	echo ""
	echo "Not enough parameters!"
	usage
	exit 1
fi
